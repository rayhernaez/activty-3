public class PasswordGenerator {
    private int passwordlength;

    public PasswordGenerator(int passlength){
        this.passwordlength = passlength;
    }

    public String generateWeakPassowrd(){
        return "weak";
    }

    public String generateStrongPassowrd(){
        return "strong";
    }

    public int getPasswordLength(){
        return this.passwordlength;
    }


}
