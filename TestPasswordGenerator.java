import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class TestPasswordGenerator {
    
    PasswordGenerator p1 = new PasswordGenerator(10);

    @Test
    public void testPassGenConstructor() {
        assertEquals(p1.getPasswordLength(), 10);
    }

    @Test
    public void testGenerateWeakPassowrd() {
        assertEquals(p1.generateWeakPassowrd().length(), 10);
    }

    @Test
    public void testGenerateStrongPassowrd() {
        assertEquals(p1.generateStrongPassowrd().length(), 10);
    }


}
